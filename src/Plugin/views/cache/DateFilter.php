<?php

namespace Drupal\vcp4dates\Plugin\views\cache;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\cache\CachePluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Caching of views data based on the value of a date filter.
 *
 * @ingroup views_cache_plugins
 *
 * @ViewsCache(
 *   id = "date_filter",
 *   title = @Translation("Date filter"),
 *   help = @Translation("Caching based on the value of a date filter.")
 * )
 */
class DateFilter extends CachePluginBase {

  /**
   * {@inheritdoc}
   */
  protected $usesOptions = TRUE;

  /**
   * {@inheritdoc}
   *
   * @param mixed $time
   *   The time service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, protected TimeInterface $time) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('datetime.time')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['date_filter'] = ['default' => NULL];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $filters = $this->view->getDisplay()->getOption('filters');
    $options = [];
    foreach ($filters as $filter) {
      if (in_array($filter['plugin_id'], ['date', 'datetime']) && in_array($filter['operator'], ['>', '>='])) {
        $options[$filter['id']] = $filter['id'];
      }
    }
    $form['date_filter'] = [
      '#type' => 'select',
      '#title' => $this->t('Date filter to use'),
      '#options' => $options,
      '#default_value' => $form_state->getValue('date_filter'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function summaryTitle() {
    return $this->options['date_filter'];
  }

  /**
   * {@inheritdoc}
   */
  protected function getLifespan() {
    $filterId = $this->options['date_filter'];
    $filter = $this->view->getDisplay()->getOption('filters')[$filterId];
    $filterValue = strtotime($filter['value']['value']);

    $query = $this->view->getQuery()->query();
    $query->addExpression($filter['table'] . '.' . $filter['field'], 'dateFilterCachePluginValue');
    $results = $query->execute()->fetchAll();

    $timestamps = [];
    foreach ($results as $result) {
      $timestamps[] = strtotime($result->dateFilterCachePluginValue);
    }
    sort($timestamps);

    return reset($timestamps) - $filterValue;
  }

  /**
   * {@inheritdoc}
   */
  protected function cacheExpire($type) {
    $lifespan = $this->getLifespan();
    if ($lifespan) {
      $cutoff = $this->time->getRequestTime() - $lifespan;
      return $cutoff;
    }
    else {
      return TRUE;
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function cacheSetMaxAge($type) {
    $lifespan = $this->getLifespan();
    if ($lifespan) {
      return $lifespan;
    }
    else {
      return 0;
    }
  }

}
